﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandgunAmmo : MonoBehaviour
{
    Gun Gun;
    // Start is called before the first frame update
    void Start()
    {
        Gun = FindObjectOfType<Gun>();
    }

    // Update is called once per frame
    void Update()
    { 
    }

    private void OnTriggerEnter(Collider player)
    {
        if (player.tag == "Player")
        {
            Gun.Total_ammo += 50;
            gameObject.SetActive(false);
        }
    }
}
