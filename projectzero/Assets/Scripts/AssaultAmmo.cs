﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssaultAmmo : MonoBehaviour
{
    gunAssault Gun;
    // Start is called before the first frame update
    void Start()
    {
        Gun = FindObjectOfType<gunAssault>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerEnter(Collider player)
    {
        if (player.tag == "Player")
        {
            Gun.Total_ammo += 60;
            gameObject.SetActive(false);
        }
    }
}
