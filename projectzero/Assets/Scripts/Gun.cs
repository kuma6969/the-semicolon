﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Gun : MonoBehaviour
{
    public float damage = 10f;
    public float range = 100f;
    public float fireRate = 10f;
    public Animator animat;
    public AudioSource gunsound;
    public Text ammoText;
    public Text ammoText2;
    public GameObject impact;
    public ParticleSystem muzzle;
    public float hitForce = 30f; 
    public int ammo1;
    public int ammo2;
    public int Total_ammo = 120;
    private float nextTimeToFire = 0f;
    private bool isReloading = false;
    public int maxAmmo = 10;
    private int currentAmmo;
    public float reloadTime = 1f;

    

    public Camera fpsCam;

    void Start()
    {
        currentAmmo = maxAmmo; 
        //g17 = FindObjectOfType<Glock17>();
        //orf = FindObjectOfType<OrangeRF>();

    }

    private void OnEnable()
    {
        isReloading = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Total_ammo >= maxAmmo)
        {
            ammo1 = currentAmmo;
            ammo2 = Total_ammo - ammo1;
        }
        ammoText.text = ammo1.ToString();
        ammoText2.text = ammo2.ToString();

        if (isReloading)
            return;

        if (currentAmmo <= 0)
        { 
            StartCoroutine(Reload());
            return;
        }
        if (animat.name == "AssaultRF")
        {
            if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire && Total_ammo > 0)
            {
                Total_ammo--;
                ammo1--;
                nextTimeToFire = Time.time + 1f / fireRate;
                Shoot();
                gunsound.Play();
                animat.SetBool("shoot", true);

            }
            if (Input.GetButtonUp("Fire1") || currentAmmo<=0)
            { 
                animat.SetBool("shoot", false);
            }
        }
        if (animat.name == "Glock17")
        {
            if (Input.GetButtonDown("Fire1") && Time.time >= nextTimeToFire && Total_ammo > 0)
            {
                Total_ammo--;
                ammo1--;
                nextTimeToFire = Time.time + 1f / fireRate;
                Shoot();
                gunsound.Play();
                animat.SetBool("shoot", true);

            }
            if (Input.GetButtonUp("Fire1"))
            {
                animat.SetBool("shoot", false);
            }
        }
    }

    IEnumerator Reload()
    { 
        isReloading = true; 
        yield return new WaitForSeconds(reloadTime); 
        currentAmmo = maxAmmo;
        isReloading = false;
    }

    void Shoot()
    {
        muzzle.Play(); 
        currentAmmo--;

        RaycastHit hit;
        if(Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {  
            ESemicolon enemy = hit.transform.GetComponent<ESemicolon>();
            if (enemy != null)
            {
                enemy.TakeDamage(damage);
            }

            if (hit.rigidbody)
            {
                hit.rigidbody.AddForce(-hit.normal*hitForce);
            }

            GameObject impactGO = Instantiate(impact, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(impactGO,2f);
        }
    }
}
