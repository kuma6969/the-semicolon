﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorController : MonoBehaviour
{
    // Start is called before the first frame update
    public Animator anim;
    public GameObject OpenPanel = null;
    public Text doorStat;
    Animation a;

    void Start()
    {
        anim = GetComponent<Animator>();
        OpenPanel.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            OpenPanel.SetActive(true);
        }

    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            OpenPanel.SetActive(false);
        }
    }

    private bool IsOpenPanelActive
    {
        get
        {
            return OpenPanel.activeInHierarchy;
        }
    }

    // Update is called once per frame
    void Update()
    { 
        if (IsOpenPanelActive)
        {
            if (Input.GetKeyDown(KeyCode.E) && anim.GetBool("open") == false)
            {
                OpenPanel.SetActive(true);
                anim.SetBool("open", true); 
            }
            else if (Input.GetKeyDown(KeyCode.E) && anim.GetBool("open") == true)
            {
                OpenPanel.SetActive(true);
                anim.SetBool("open", false);
            }

            if (anim.GetBool("open") == false)
            {
                doorStat.text = "Press E to open";
            }
            else
            {
                doorStat.text = "Press E to close";
            }
        }
    }
}
