﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public float health = 50;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame

    public void DeductPoints(float dmgAmnt)
    {
        health -= dmgAmnt;
    }
    void Update()
    { 
        if (health <= 0)
        {
            Destroy(gameObject);
        }   
    }
}
